Require translation
-------------------
The module adds "Translation required" to the administration node content page
(admin/content/node) filter options. This enables editors to mark nodes that
needs translation, so translators easy can get a overview of these nodes.

The require translation option is avaliable on the node edit form for nodes that
support translation.


Installation
------------
Download the module and enable it as any other Drupal module.


Requirements
------------
The module requires that the Drupal core translation module is enabled.


